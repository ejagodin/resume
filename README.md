# resume

ERIC JAGODINSKI
ejagodin@fau.edu


EDUCATION

Florida Atlantic University (FAU)									                    Boca Raton, FL
Ph.D.* in Ocean Engineering									                            Summer 2017-Present
Field of Research: Using machine learning for manipulation of turbulent fluid flow

Florida Atlantic University (FAU) 								                        Boca Raton, FL	 
Master of Science in Ocean Engineering 								                    August 2019
Graduate Certificate in Offshore Engineering

Florida Atlantic University (FAU) 								                        Boca Raton, FL	 
Bachelor of Science in Ocean Engineering 								                Graduated May 2016

Undergraduate Capstone Design Project, Electrical Team Lead					
Autonomous surface vehicle capable of GPS navigation and station keeping	 		    Fall 2015-Spring 2016


ENGINEERING EXPERIENCE
Naval Research Laboratory, Stennis Space Center, Graduate Intern					    Stennis, MS
Numerical Modeling of Rogue Wave Phenomenon							                    Summer 2018
Using OpenFOAM VoF solver, developed simulations for rogue wave and wind interaction
Developed a turbulence closure model modification addressing unnatural energy dissipation

ZUtA Labs, Electrical Engineering Summer Intern							                Jerusalem, Israel
ZUtA created the first ever portable robotic printer that drives on top of the paper.	Summer 2015
Designed experiments for choosing production model parts and created a monitoring 
system for position feedback

Agilis Measurement Systems, Technician								                    Palm Beach Gardens, FL 	
Real time and offline non-intrusive turbine blade stress analysis.						2014 - 2015
Assembled complex computer monitoring and signal conditioning systems used on power 
generation turbines and wrote monthly reports for the whole state. Assimilated separate laser 
and detector systems.


TEACHING
Sylvan Learning Center, Tutor									                        Ponte Vedra, FL  		
Math, Science and SAT tutor for high school students							        2017

Private Tutor
Math, Engineering undergraduate                                                         2012-2018


SKILLS

Neural Networks and Reinforcement Learning with Keras, Tensorflow
Extensive data analysis using Python and Matlab
CFD: DNS, OpenFOAM and Waves2Foam
CADs: Solid modeling, assemblies and drafting prints for fabrication using SolidWorks, AutoCAD and Inventor 
Electrical design,assembly and testing


